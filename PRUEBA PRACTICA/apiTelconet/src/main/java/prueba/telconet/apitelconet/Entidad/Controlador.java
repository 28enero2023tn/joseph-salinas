/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prueba.telconet.apitelconet.Entidad;

import java.beans.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author USUARIO
 */
public class Controlador {
    Connection connection = null;
    
    public void connectDatabase() {
        try {
            // We register the PostgreSQL driver
            // Registramos el driver de PostgresSQL
            try { 
                Class.forName("org.postgresql.Driver");
            } catch (ClassNotFoundException ex) {
                System.out.println("Error al registrar el driver de PostgreSQL: " + ex);
            }
            
            // Database connect
            // Conectamos con la base de datos
            connection = DriverManager.getConnection(
                    "jdbc:postgresql://localhost:5432/bd_local",
                    "usuariobd", "1234");
            
            
 
            boolean valid = connection.isValid(50000);
            System.out.println(valid ? "TEST OK" : "TEST FAIL");
        } catch (java.sql.SQLException sqle) {
            System.out.println("Error: " + sqle);
        }
    } 
    /**
     * Testing Java PostgreSQL connection with host and port
     * Probando la conexión en Java a PostgreSQL especificando el host y el puerto.
     * @param args the command line arguments
     */
    public Controlador() {
        Controlador javaPostgreSQLBasic = new Controlador();
        javaPostgreSQLBasic.connectDatabase(); 
        
    }
    
    
    public void ingresarTurno(String nombre, Long area, Long tramite, String observacion ) throws SQLException{
        
        
            String sql = "insert into turnos(nombre, tramite_id, area_id, observacion_cli,estado_turno,estado) values(?,?,?,?,?,?)";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, nombre);
            ps.setLong(2, area);
            ps.setLong(3, tramite);            
            ps.setString(4, observacion);
            ps.setString(5, "PENDIENTE");
            ps.setString(6, "A");


            ps.executeUpdate();
            
            
    }

   
}
    

