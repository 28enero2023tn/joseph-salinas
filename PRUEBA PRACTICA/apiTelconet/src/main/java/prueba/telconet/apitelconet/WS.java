/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/WebServices/EjbWebService.java to edit this template
 */
package prueba.telconet.apitelconet;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import prueba.telconet.apitelconet.Entidad.Controlador;

/**
 *
 * @author USUARIO
 */
@WebService(serviceName = "WS")
@Stateless()
public class WS {

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "hello")
    public String hello(@WebParam(name = "name") String txt) {
        return "Hello " + txt + " !";
    }
    
    
    @WebMethod(operationName = "insertarTurno")
    public String registrarTurno(@WebParam(name = "name") String nombre,
                        @WebParam(name = "area") Long area,
                        @WebParam(name = "tramite") Long tramite,
                        @WebParam(name = "observacion") String observacion) {
        
        Controlador ctrl = new Controlador();
        try {
            ctrl.ingresarTurno(nombre, area, tramite, observacion);
        } catch (SQLException ex) {
            Logger.getLogger(WS.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Registro OK!";
    }
    
    
}
