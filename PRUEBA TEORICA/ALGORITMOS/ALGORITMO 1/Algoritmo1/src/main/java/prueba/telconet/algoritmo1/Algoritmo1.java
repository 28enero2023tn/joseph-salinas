package prueba.telconet.algoritmo1;

/**
 *
 * @author JOSEPH SALINAS
 */
public class Algoritmo1 {

//Si la carta anterior fue un número, la siguiente debe ser una figura
//Si la carta anterior fue una figura, la siguiente debe ser un número
//Diferente Palo
/*    
    3 CORAZÓN_ROJO
    K TREBOL
    7 DIAMANTE
    AS TREBOL
    5 CORAZON_NEGRO
*/

    //variables
        String[] palos = {"CORAZON_NEGRO", "CORAZON_ROJO", "DIAMANTE", "TREBOL"};
        String[] cartas = {"numero","figura"};
        int [] numeros = {2, 3, 4, 5, 6, 7, 8, 9, 10};
        String[] figuras = {"J", "Q", "K", "AS"};

    
    public static void main(String[] args) {
        
        String resultadoCarta = "";
        int numero_cartas = 10; //variable de definicion de numero de cartas

        Algoritmo1 alg1 = new Algoritmo1(); //instancia de clase para usar metodos
        
        //variables para no repetir el palo generado
        String palo = "";
        String paloGenerado = "";
        //variables para no repetir las cartas generadas
        String carta = "";
        String cartaGenerado = "";


        for (int i = 1; i <= numero_cartas; i++) { //contador de X cartas
            
            //do-while para generar una carta y que esta no sea igual a la anterior
            do {
                cartaGenerado = alg1.generarCarta();//accede al metodo para generar una carta
            }
            while (carta.equals(cartaGenerado));//valida que la carta no sea igual - recordar que se sale en false
            carta = cartaGenerado;// asignacion del valor nuevo de la carta
            System.out.println(carta);
            
            //validacion para saber si es figura o numero para que no se repitan
            if(carta.equals("numero")){
                resultadoCarta = String.valueOf(alg1.generarNumero());//generamos un numero aleatorio
            }else if(carta.equals("figura")){
                resultadoCarta = alg1.generarfigura();//genramos una figura aleatoria
            }

            //do-while para validar que no se repita el mismo palo anterior
            do {
            paloGenerado = alg1.generarPalo();//generamos un nuevo palo
            }
            while (palo.equals(paloGenerado));//valida que el palo no sea igual - recordar que se sale en false
            palo = paloGenerado;//asignamos el nuevo palo


            System.out.println(resultadoCarta + " " + palo); //imprimimos el resultado final de la carta y el palo asignado

        }
            
           
        
        
    }
    
    //funciones para generar un PALO aleatorio
    public String generarPalo(){
        int palo = (int) (Math.random() * 4 + 1);
        return  palos[palo-1];     
    }
    
    //funciones para generar un CARTA aleatorio
    public String generarCarta(){
        int carta = (int) (Math.random() * 2 + 1);
        return  cartas[carta-1]; 
    }
    
    //funciones para generar un NUMERO aleatorio
    public int generarNumero(){
        int numero = (int) (Math.random() * 9 + 1);
        return  numeros[numero-1];     
    }
    
    //funciones para generar un FIGURA aleatorio
    public String generarfigura(){
        int figura = (int) (Math.random() * 4 + 1);
        return  figuras[figura-1];     
    }
    
    
    
}
