package prueba.telconet.algoritmo2;

import java.util.Scanner;

/**
 *
 * @author JOSEPH SALINAS
 */
public class Algoritmo2 {

    /*
-	Si tiene 10 dígitos es CEDULA
-	Si tiene 13 dígitos es RUC
-	Si empieza con 09, indicar que es de Guayas, caso contrario Otra Provincia
-	SI no cumple lo anterior, se debe mostrar que la cédula no es válida
    
    1.	0900000001 
    CÉDULA DE GUAYAQUIL
    2.	09001
    IDENTIFICACIÓN INVALIDA
    3.	0900000001001
    RUC DE GUAYAQUIL
    4.	09000000000000001
    IDENTIFICACIÓN INVALIDA
    5.	1700000001
    CEDULA DE OTRA PROVINCIA
    6.	1700000000001
    RUC DE OTRA PROVINCIA

    */
    
    
    public static void main(String[] args) {
        
        
        System.out.println("Por favor ingrese su numero de CEDULA/RUC: ");
        //Clase para obtener valores
        Scanner sc = new Scanner(System.in); 
        String dni = sc.nextLine();// obtine el valor ingresado hasta dar ENTER
        
        Algoritmo2 alg2 = new Algoritmo2();//instancia de Clase para utilizar metodos
        
        String cedula_ruc = alg2.validaCEDRUC(dni);//validamos si es cedula o RUC
        
        if(!cedula_ruc.equals("ERROR")){//validamos que no sea erronea
            
            String esGuayas = alg2.validaGYS(dni);//validamos si es GYE
            
            System.out.println(cedula_ruc + esGuayas);// imprimimos mensaje de validacion de cedula/ruc  + validacion de Guayas
            
        }else{//si es erronea
            System.out.println("CEDULA NO VALIDA");
        }
        
        
    }
    
    //metodo de validacion de Cedula o RUC mediante length()
    public String validaCEDRUC(String dni){
        String respuesta = "";
        if(dni.length() == 10){
            respuesta = "CEDULA";
        }else if(dni.length() == 13){
            respuesta = "RUC";
        }else{
            respuesta = "ERROR";
        }
        return respuesta;
    }
    
    //validacion de guayas - validacion mediante substring para obtener primeros digitos
    public String validaGYS(String dni){
        String numero = dni.substring(0, 2);
        if(numero.equals("09")){
            return " DE GUAYAS";
        }else{
            return " DE OTRA PROVINCIA";
        }
    }
    
}
